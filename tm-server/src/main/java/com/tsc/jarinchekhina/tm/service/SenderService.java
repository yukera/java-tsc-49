package com.tsc.jarinchekhina.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tsc.jarinchekhina.tm.api.service.ISenderService;
import com.tsc.jarinchekhina.tm.dto.EntityLogDTO;
import com.tsc.jarinchekhina.tm.enumerated.EntityActionType;
import com.tsc.jarinchekhina.tm.listener.EntityListener;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;

import javax.jms.*;
import java.util.Date;

/**
 * SenderService
 *
 * @author Yuliya Arinchekhina
 */
public class SenderService implements ISenderService {

    @NotNull
    final ConnectionFactory connectionFactory =
            new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);

    @Override
    @SneakyThrows
    public void send(@NotNull final EntityLogDTO entity) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createQueue("TM_QUEUE");
        @NotNull final MessageProducer producer = session.createProducer(destination);
        @NotNull final ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        @NotNull final String jsonEntity = objectWriter.writeValueAsString(entity);
        @NotNull final TextMessage message = session.createTextMessage(jsonEntity);
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
    }

    @Override
    @SneakyThrows
    public EntityLogDTO createMessage(@NotNull final Object object, @NotNull final EntityActionType actionType) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String entityDTO = objectMapper.writeValueAsString(object);
        @NotNull final String entityName = object.getClass().getSimpleName();
        @NotNull final EntityLogDTO message =
            EntityLogDTO.builder()
                        .formattedDate(String.format("%td-%<tM-%<ty %<tR:%<tS", new Date()))
                        .className(entityName)
                        .actionType(actionType)
                        .entityDTO(entityDTO)
                        .build();
        return message;
    }

}
