package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.api.service.ISenderService;
import com.tsc.jarinchekhina.tm.dto.EntityLogDTO;
import com.tsc.jarinchekhina.tm.enumerated.EntityActionType;
import com.tsc.jarinchekhina.tm.service.SenderService;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * MessageExecutor
 *
 * @author Yuliya Arinchekhina
 */
public class MessageExecutor {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final ISenderService senderService = new SenderService();

    @NotNull
    private final ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull final EntityActionType actionType) {
        executorService.submit(() -> {
            @NotNull final EntityLogDTO entity = senderService.createMessage(object, actionType);
            senderService.send(entity);
        });
    }

    public void stop() {
        executorService.shutdown();
    }

}
