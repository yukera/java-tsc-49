package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;

public interface IConnectionProvider extends IServiceLocator {
}
