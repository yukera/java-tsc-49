package com.tsc.jarinchekhina.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.jarinchekhina.tm.api.ILoggerService;
import com.tsc.jarinchekhina.tm.dto.EntityLogDTO;
import com.tsc.jarinchekhina.tm.service.LoggerService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class LoggerListener implements MessageListener {

    @NotNull
    final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NotNull final String jsonEntity = ((TextMessage) message).getText();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final EntityLogDTO entityLogDTO = mapper.readValue(jsonEntity, EntityLogDTO.class);
        loggerService.writeLog(entityLogDTO);
    }

}
