package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.ILoggerService;
import com.tsc.jarinchekhina.tm.dto.EntityLogDTO;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.tsc.jarinchekhina.tm.constant.DataConstant.*;
import static com.tsc.jarinchekhina.tm.constant.ObjectClassConstant.*;

/**
 * LoggerService
 *
 * @author Yuliya Arinchekhina
 */
public class LoggerService implements ILoggerService {

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final EntityLogDTO message) {
        @Nullable final String className = message.getClassName();
        @Nullable final String fileName = getFileName(className);
        if (fileName == null) return;
        @NotNull final File file = new File(fileName);
        if (!file.exists()) Files.createFile(Paths.get(file.getAbsolutePath()));
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        @NotNull final String header = "Id: " + message.getId()
                + "; Type: " + message.getActionType()
                + "; Date: " + message.getFormattedDate() + ";\n";
        fileOutputStream.write(header.getBytes());
        fileOutputStream.write(message.getEntityDTO().getBytes());
        fileOutputStream.write(";\n".getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    private String getFileName(@NotNull final String className) {
        switch (className) {
            case PROJECT:
            case PROJECT_DTO:
                return PROJECT_LOG;
            case TASK:
            case TASK_DTO:
                return TASK_LOG;
            case USER:
            case USER_DTO:
                return USER_LOG;
            case SESSION:
            case SESSION_DTO:
                return SESSION_LOG;
            default:
                return null;
        }
    }

}
