package com.tsc.jarinchekhina.tm.constant;

public class ObjectClassConstant {

    public static final String PROJECT = "Project";

    public static final String TASK = "Task";

    public static final String USER = "User";

    public static final String SESSION = "Session";

    public static final String PROJECT_DTO = "ProjectDTO";

    public static final String TASK_DTO = "TaskDTO";

    public static final String USER_DTO = "UserDTO";

    public static final String SESSION_DTO = "SessionDTO";

}
