package com.tsc.jarinchekhina.tm.constant;

public class DataConstant {

    public static final String PROJECT_LOG = "./project.log";

    public static final String TASK_LOG = "./task.log";

    public static final String USER_LOG = "./user.log";

    public static final String SESSION_LOG = "./session.log";

}
