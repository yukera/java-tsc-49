package com.tsc.jarinchekhina.tm.command.user;

import com.tsc.jarinchekhina.tm.command.AbstractUserCommand;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserByLoginUnlockCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "unlock user by login";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getAdminUserEndpoint().unlockByLogin(serviceLocator.getSession(), login);
    }

}
